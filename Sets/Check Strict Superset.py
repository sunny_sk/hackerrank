a = set(map(int, raw_input().split()))
num = int(raw_input())
strict = True
for i in range(num):
	b = set(map(int, raw_input().split()))
	if not(b.issubset(a)):
		strict = False
	if len(b) >= len(a):
		strict = False

print strict