from itertools import permutations

length = int(raw_input().strip())
sample = map(str, raw_input().strip().split(" "))
lowthen = int(raw_input().strip())

count = 0
perm_len = 0
for i in permutations(sample, lowthen):
	perm_len += 1
	count += 'a' in i[:length]

print count/float(perm_len)