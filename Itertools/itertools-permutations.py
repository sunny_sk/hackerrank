from itertools import permutations

sample,num = raw_input().strip().split(" ")

sample = map(str, sample)
num = int(num)

perm = list(permutations(sample,num))

perm.sort()

for i in perm:
	print("".join(i))