from itertools import combinations

sample, num = raw_input().strip().split(" ")

sample = map(str, sample)
num = int(num)

sample.sort()

for i in range(1,num+1):
	comb = list(combinations(sample, i))
	comb.sort()
	for j in comb:
		print("".join(j))
