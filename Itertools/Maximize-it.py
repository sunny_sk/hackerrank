from __future__ import division
import itertools

num, mod = raw_input().strip().split(" ")

arr = list()
for i in range(int(num)):
	tmp = map(int, raw_input().strip().split(" "))
	arr.append(tmp[1:])

smax = 0
for l in itertools.product(*arr):
	s = sum([x**2 for x in l]) % int(mod)

	if (s > smax):
		smax = s

print smax