from itertools import combinations_with_replacement

sample, num = raw_input().strip().split(" ")

sample = map(str, sample)
num = int(num)

sample.sort()
comb2 = list(combinations_with_replacement(sample, num))

for i in comb2:
	print("".join(i))
