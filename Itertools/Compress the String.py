from __future__ import print_function
from itertools import groupby

sample = raw_input().strip()

group = groupby(sample)

out = list()
for i in group:
	c = int(i[0])
	x = 0
	for j in i[1]:
		x += 1
	out.append(tuple((x, c)))
	
[print(i, end=' ') for i in out]