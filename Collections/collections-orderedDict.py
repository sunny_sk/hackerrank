from collections import OrderedDict

items = OrderedDict()
for i in range(int(raw_input().strip())):
	item, space, price = raw_input().strip().rpartition(" ")
	items[item] = items.get(item, 0) + int(price)
	
for i in items:
	print i, items.get(i)