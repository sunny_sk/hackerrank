from collections import defaultdict

n, m = map(int, raw_input().split())
sample = defaultdict(list)
for i in range(1,n+1):
	s = raw_input().strip()
	sample[s].append(i)

for i in range(m):
	s = raw_input().strip()
	if s in sample:
		print(' '.join(map(str, sample[s])))
	else:
		print('-1')