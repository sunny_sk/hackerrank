from collections import Counter

numOfShoes = int(raw_input().strip())
AvailableShoes = Counter(map(int, raw_input().strip().split(" ")))
numOfCust = int(raw_input().strip())

profit = 0
for i in range(numOfCust):
 	shoes, price = raw_input().strip().split(" ")
 	if AvailableShoes.has_key(int(shoes)) and AvailableShoes[int(shoes)] > 0:
 		AvailableShoes[int(shoes)] -= 1
 		profit += int(price)

print profit
