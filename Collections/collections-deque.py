from collections import deque

deq = deque()
for i in range(int(raw_input().strip())):
	cmd = raw_input().strip().split(" ")
	if(cmd[0] == 'pop'):
		deq.pop()
	else :
		if(cmd[0] == 'popleft'):
			deq.popleft()
		else:
			eval("deq."+cmd[0]+"("+cmd[1]+")")

print " ".join(map(str, deq))