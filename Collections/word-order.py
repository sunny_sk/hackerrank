from collections import OrderedDict

items = OrderedDict()
for i in range(int(raw_input().strip())):
	item = raw_input().strip()
	items[item] = items.get(item, 0) + 1

print len(items)
print " ".join(map(str, items.values()))